using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;
using Proxy.Api.Domain;
using Serilog;

namespace Proxy.Api
{
    public class Startup
    {
	    private readonly string applicationName;
	    
	    public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
            applicationName = typeof(Program).Assembly.GetName().Name;
        }

        private readonly IConfiguration configuration;
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddRouting(options => options.LowercaseUrls = true);
            services.AddHttpContextAccessor();

            services.AddTransient<IProxyManager, ProxyManager>();
            
            if (configuration.GetValue<bool>("EnableSwagger"))
	            services.AddSwaggerGen(c =>
	            {
		            c.SwaggerDoc("v1", new OpenApiInfo { Title = applicationName, Version = "v1" });
		            c.IncludeXmlComments(Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, $"{applicationName}.xml"));
	            });

			Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();
		}

        public void Configure(IApplicationBuilder app, IHostApplicationLifetime applicationLifetime)
        {
	        applicationLifetime.ApplicationStopping.Register(OnApplicationStopping);

	        app.UseRouting();
	        app.UseEndpoints(endpoints => endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}"));

	        if (configuration.GetValue<bool>("EnableSwagger"))
	        {
		        app.UseSwagger();
		        app.UseSwaggerUI(c =>
		        {
			        c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{applicationName} v1");
			        c.RoutePrefix = string.Empty;
		        });
	        }
            
	        Log.Logger.Information($"{applicationName} has been started");
        }

        private void OnApplicationStopping()
        {
	        Log.Logger.Information($"{applicationName} has been stopped");
        }
	}
}