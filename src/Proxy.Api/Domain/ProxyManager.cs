﻿using System;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Proxy.Api.Domain
{
    public interface IProxyManager
    {
        Task<IActionResult> ProxyRequest();
    }

    public class ProxyManager : IProxyManager
    {
        private readonly ILogger<ProxyManager> logger;
        private readonly IHttpContextAccessor httpContextAccessor;

        public ProxyManager(ILogger<ProxyManager> logger, IHttpContextAccessor httpContextAccessor)
        {
            this.logger = logger;
            this.httpContextAccessor = httpContextAccessor;
        }
        
        public async Task<IActionResult> ProxyRequest()
        {
            var request = httpContextAccessor.HttpContext.Request;
            var endpoint = request.Headers["X-Destination-Endpoint"];
            var thumbprint = request.Headers["X-Certificate-Thumbprint"].ToString();
        
            var store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);

            var certificates = store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, true);
            if (certificates.Count == 0)
            {
                logger.LogError($"No certificate found with thumbprint {thumbprint}. Destination endpoint: {endpoint}");
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }
        
            var handler = new HttpClientHandler();
            handler.ClientCertificates.Add(certificates[0]);
            using var client = new HttpClient(handler);
            var requestUri = new Uri(endpoint);

            var proxyRequest = new HttpRequestMessage(new HttpMethod(request.Method), requestUri)
                {Content = new StreamContent(request.Body)};
            var response = await client.SendAsync(proxyRequest);
            return new HttpResponseMessageResult(response);
        }
    }
}