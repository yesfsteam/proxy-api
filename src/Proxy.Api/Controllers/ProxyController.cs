﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Proxy.Api.Domain;

namespace Proxy.Api.Controllers
{
    [Route("")]
    public class ProxyController : ControllerBase
    {
        private readonly IProxyManager manager;

        public ProxyController(IProxyManager manager)
        {
            this.manager = manager;
        }
        
        /// <summary>
        /// Отправляет запрос с сертификатом
        /// </summary>
        public async Task<IActionResult> ProxyRequest1()
        {
            return await manager.ProxyRequest();
        }
    }
}